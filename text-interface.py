from datetime import *

print("Welcome to Bird interface")
print("Print help to  get general commands")


def help_graph():
    print("Enter exit to leave ")
    print("Enter new input to start input sequence")


# start date
# number of dates
# intervals - total per hour, total per day, total per week

last_command = " "
while last_command != "exit":
    last_command = input()
    
    if last_command == "help":
        help_graph()

    if last_command == "new input" or "enter input" or "input":
        strDate = input("Enter new start date. \n Example of valid input: 2015-08-5 9:31")

        dateForm = '%Y-%m-%d:%H:%M'
        date = datetime.timestamp(datetime.strptime(strDate, dateForm))

        number_of_dates = input("Input number of dates.")
        try:
            number_of_dates = int(number_of_dates)
        except:
            raise ValueError("Invalid input.")

        intervals = input("Interval, total per hour(1), total per day(2), total per week(3).")
        try:
            intervals = int(intervals) 
        except:
            raise ValueError("Invalid input.")
        
        valid = [1, 2, 3]
        if intervals not in valid:
            raise ValueError("Invalid intervals.")


        

